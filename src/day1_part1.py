import pathlib
from itertools import pairwise
lines = pathlib.Path('input_test2.txt').read_text().rstrip().split('\n')
print(lines)
numbers = [int(line) for line in lines]
print(numbers)
for a, b in pairwise(numbers):
    print(a,b, b > a)
increases = [b > a for a, b in pairwise(numbers)]
print(increases)
print(sum(increases))